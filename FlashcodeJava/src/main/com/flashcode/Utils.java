package com.flashcode;

import com.flashcode.models.atOwnRisk.Coordinate;

public class Utils {
    private Utils() {
    }

    public static int calcDistance(Coordinate start, Coordinate end) {
        var distance = 0;


        distance += Math.abs(start.column - end.column);
        distance += Math.abs(start.row - end.row);
        return distance;
    }
}
