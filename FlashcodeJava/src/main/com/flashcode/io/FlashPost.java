package com.flashcode.io;

import com.flashcode.models.atOwnRisk.AssignmentDetails;
import com.flashcode.models.atOwnRisk.InputModel;
import com.flashcode.models.atOwnRisk.VehicleAssignment;
import com.google.gson.Gson;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.List;

import static com.flashcode.Main.ApiKey;
import static com.flashcode.Main.BaseUri;

public class FlashPost {
    private final HttpClient client = HttpClients.createDefault();
    private final Gson gson = new Gson();

    public AssignmentDetails[] getAssignmentIds() throws IOException {
        var get = new HttpGet(BaseUri + "/api/problem");
        get.addHeader("apikey", ApiKey);

        var response = client.execute(get);
        if (!isOk(response.getStatusLine())) {
            System.out.println("Get assignments: " + response.getStatusLine().toString());
        }
        var entity = response.getEntity();

        if (entity != null) {
            try (var br = new BufferedReader(new InputStreamReader(entity.getContent()))) {
                StringBuilder sb = new StringBuilder();
                String str;
                while ((str = br.readLine()) != null) {
                    sb.append(str);
                }
                return gson.fromJson(sb.toString(), AssignmentDetails[].class);
            }
        }

        return new AssignmentDetails[0];
    }

    public InputModel getAssignmentById(String id) throws IOException {
        var get = new HttpGet(BaseUri + "/api/problem/" + id);
        get.addHeader("apikey", ApiKey);

        var response = client.execute(get);
        if (!isOk(response.getStatusLine())) {
            System.out.println("Get assignment by id: " + response.getStatusLine().toString());
        }
        var entity = response.getEntity();

        if (entity != null) {
            try (var br = new BufferedReader(new InputStreamReader(entity.getContent()))) {
                StringBuilder sb = new StringBuilder();
                String str;
                while ((str = br.readLine()) != null) {
                    sb.append(str);
                }
                var resultString = sb.toString()
                        .trim()
                        .replace("\"", "");
                var bytes = Base64.getDecoder().decode(resultString);
                resultString = new String(bytes, StandardCharsets.UTF_8);
//            var content = Encoding.UTF8.GetString(response).Trim();
                return Mapper.MapInput(resultString.split("\n"));
            }
        }
        throw new NullPointerException("No entity received from server");
    }

    public int submitAssignment(String id, List<VehicleAssignment> output) throws IOException {
        var outputString = Mapper.MapOutput(output);
        var post = new HttpPost(BaseUri + "/api/submit/" + id);
        post.addHeader("apikey", ApiKey);

        var base64String = "\"" + Base64.getMimeEncoder().encodeToString(outputString.getBytes(StandardCharsets.UTF_8)) + "\""; // ASP.net core is a bit of a weird kid
        post.setEntity(new StringEntity(base64String));
        post.addHeader("Content-type", "application/json; charset=utf-8");

        var response = client.execute(post);
        if (!isOk(response.getStatusLine())) {
            System.out.println("Submit: " + response.getStatusLine().toString());
        }

        var entity = response.getEntity();

        if (entity != null) {
            try (var br = new BufferedReader(new InputStreamReader(entity.getContent()))) {
                StringBuilder sb = new StringBuilder();
                String str;
                while ((str = br.readLine()) != null) {
                    sb.append(str);
                }
                var resultString = sb.toString();
                return Integer.parseInt(resultString);
            }
        }
        throw new NullPointerException("No entity received from server");
    }

    private boolean isOk(StatusLine statusLine) {
        if (statusLine.getStatusCode() / 100 == 2) {
            return true;
        }
        return false;
    }
}
