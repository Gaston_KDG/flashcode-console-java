package com.flashcode;

import com.flashcode.io.FlashPost;
import com.flashcode.models.atOwnRisk.InputModel;
import com.flashcode.models.atOwnRisk.VehicleAssignment;
import org.apache.commons.lang3.time.StopWatch;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static String BaseUri = "http://judgesystem.azurewebsites.net";
    public static String ApiKey = "API KEY HERE";

    public static void main(String[] args) throws IOException {
        var totalTimer = new StopWatch();
        var watch = new StopWatch();
        var totalScore = 0;
        watch.start();
        watch.stop();

        var client = new FlashPost();
        var assignmentDetails = client.getAssignmentIds();

        totalTimer.start();
        for (var assignmentDetail : assignmentDetails) {
            var assignment = client.getAssignmentById(assignmentDetail.id);
            watch.reset();
            watch.start();
            var output = processInput(assignment);
            watch.stop();
            var score = client.submitAssignment(assignmentDetail.id, output);
            System.out.println(assignmentDetail.name + ": " + watch.formatTime() + " ms Score: " + score);
            totalScore += score;
        }

        totalTimer.stop();
        System.out.println("Total Time: " + totalTimer.formatTime());
        System.out.println("Total score: " + totalScore);
    }


    private static List<VehicleAssignment> processInput(InputModel input) {
        var output = new ArrayList<VehicleAssignment>(); // fill me up pls
        for (int i = 0; i < input.vehicles; i++)
        {
            output.set(i, new VehicleAssignment());
        }

        /*
         * Todo: Put Code here
         */

        return output;
    }
}


