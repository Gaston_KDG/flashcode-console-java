package com.flashcode.models;

import com.flashcode.models.atOwnRisk.Coordinate;
import com.flashcode.models.atOwnRisk.RideDefinition;

import java.util.LinkedList;
import java.util.List;

public class Vehicle {
    public int stepsTaken;
    public List<RideDefinition> rides;
    public Coordinate position;

    public Vehicle() {
        rides = new LinkedList<>();
        stepsTaken = 0;
        position = new Coordinate(0, 0);
    }
}
