package com.flashcode.models.atOwnRisk;

public class InputModel {
    public int rows;
    public int columns;
    public int vehicles;
    public int rideCount;
    public int bonusPoints;
    public int steps;
    public RideDefinition[] rideDefinitions;

    public InputModel(int rows, int columns, int vehicles, int rideCount, int bonusPoints, int steps) {
        this.rows = rows;
        this.columns = columns;
        this.vehicles = vehicles;
        this.rideCount = rideCount;
        this.bonusPoints = bonusPoints;
        this.steps = steps;
    }
}