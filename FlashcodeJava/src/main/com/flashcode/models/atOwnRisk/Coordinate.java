package com.flashcode.models.atOwnRisk;

import java.util.Objects;

public class Coordinate
    {
        public Coordinate(int row, int column)
        {
            this.row = row;
            this.column = column;
        }

        public final int row;
        public final int column;

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Coordinate that = (Coordinate) o;
            return row == that.row && column == that.column;
        }

        @Override
        public int hashCode() {
            return Objects.hash(row, column);
        }
    }