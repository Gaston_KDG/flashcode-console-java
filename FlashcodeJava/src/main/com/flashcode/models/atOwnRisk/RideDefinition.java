package com.flashcode.models.atOwnRisk;

import com.flashcode.Utils;

public class RideDefinition {
    public final int rideId;
    public final Coordinate startCoordinate;
    public final Coordinate endCoordinate;
    public final int earliestStart;
    public final int latestFinish;
    public final int distance;

    public RideDefinition(int rideId, Coordinate startCoordinate, Coordinate endCoordinate, int earliestStart, int latestFinish) {
        this.rideId = rideId;
        this.startCoordinate = startCoordinate;
        this.endCoordinate = endCoordinate;
        this.earliestStart = earliestStart;
        this.latestFinish = latestFinish;
        distance = Utils.calcDistance(startCoordinate,endCoordinate);
    }
}